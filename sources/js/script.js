import Swiper from 'swiper/swiper-bundle';
import lightbox from 'lightbox2/dist/js/lightbox';


(function ($) {
    /*header*/
    let topSearchForm = $('.l-header__search-form');
    $('.js-open-search').on('click', function () {
        topSearchForm.addClass('active');
    });

    $('.js-close-search').on('click', function () {
        topSearchForm.removeClass('active');
    });

    $('.l-header__burger').on('click', function () {
        if($(this).hasClass('active')) {
            $(this).removeClass('active');
            $('.l-header__nav, .l-header__menu').removeClass('show');
            $('body').removeClass('modal-open');
        } else {
            $(this).addClass('active');
            $('.l-header__nav, .l-header__menu').addClass('show');
            $('body').addClass('modal-open');
        }
    });

    $('#catalog > a').on('click', function (event) {
        event.preventDefault();
        if($(this).next().hasClass('show')){
            $(this).next().removeClass('show');
        } else {
            $(this).next().addClass('show');
        }
    });

    $('.l-catalog > .dropdown').on('click', function () {
        let menu = $(this).find('.dropdown-menu');
        if(menu.hasClass('show')){
            console.log('1');
            menu.removeClass('show');
        } else {
            console.log('0');
            menu.addClass('show');
        }
    });



    /*product gallery*/
    var productThumbs = new Swiper(".product-thumbs", {
        loop: true,
        spaceBetween: 15,
        slidesPerView: 5,
        freeMode: true,
        watchSlidesProgress: true,
        direction: "vertical",
    });

    var productSlider = new Swiper(".product-slider", {
        loop: true,
        slidesPerView: 1,
        spaceBetween: 10,
        navigation: {
            nextEl: ".product-slider-next",
            prevEl: ".product-slider-prev",
        },
        pagination: {
            el: '.product-slider-pagination',
            type: 'bullets',
            clickable: 'true',
        },
        thumbs: {
            swiper: productThumbs,
        },
    });

    /*end product gallery*/

    /**/
    lightbox.option({
        'resizeDuration': 200,
        'wrapAround': true
    })
    /**/

    /*btns*/
    $('.js-add-fav').on('click', function () {
        $(this).toggleClass('active');
    })

    /*ens btns*/
})(jQuery)
